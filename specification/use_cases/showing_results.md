# Showing results

**Description**: The player selects a button to display the final result from the main menu. The application displays the final results of the players.

**Actors**:
	-  Player - selects this option from the main menu.

**Preconditions**: The application is running. The main menu is open.

**Postconditions**: Information on the final results is displayed.

**Main scenario**:
1. The player selects a button to display the results from the main menu.


2. The application displays the results in the form of a list, on which the players are ranked.


3. The player selects a button to return to the main menu.


4. The application displays the main menu.


**Alternative scenarios**: Unexpected application exit. If the user deactivates the application at any step, all stored information about the results remains unchanged. The application ends. The use case is over.

**Subscenarios**: /

**Special requirements**: /

**Additional information**: / 	
