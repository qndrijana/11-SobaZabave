# Playing one game party _Mice_

**Description**: The player starts the game from the main menu. The application loads game settings and starts the game. When the game is over, application stores information about the game and returns tokens, player result and list of the best results.

**Actors**:
	- The Player: plays one game.

**Preconditions**: The application is launched and the Mice Game is selected.

**Postconditions**: The game is completed, game data is saved and tokens are updated.

**Main scenario**:

1. The Player starts the game by pressing the *"Start"* button.

2. The application receives settings.

3. The application creates new game and starts the game.

4. While the game is not over, the following steps are repeated:  
    1. The application checks if the player has 2 figures on the board and none in his hand.
        1. If the player has 2 figures on the board and none in his hand.
            1. The application saves tokens.
            2. The application saves result of the game (*" The Player on the move lost the game "*).
            3. The application proceeds to step 5.

    2. The application checks if both players have 3 figures on the board and neither in their hand.
        1. If players have 3 figures on the board and neither in their hand.
            1. The application saves tokens.
            2. The application saves result of the game (*" Draw Game "*).
            3. The application proceeds to step 5.

    3. The application checks if the player has 3 figures on the board and none in his hand.
        1. If the player has 3 figures on the board and none in his hand.
            1. The application allows player *"Flying moves"* (Player can move his figures by skipping others).
            2. The player plays his move.

    4. The application checks if the player has more than 3 figures on the board and none in his hand.
        1. If the player has more than 3 figures on the board and none in his hand.
            1. The player plays his move.

    5. The application checks if the player has some figures in his hand.        
        1. If the player has some figures in his hand.
            1. The player puts one figure on the board.

    6. The application checks if player has 3 figures in a row.
        1. If the player has 3 figures in a row.
            1. The player chooses one opponent's figure and takes it.

    7. The application changes player on the move.

5. The application shows the result of the game

6. The application shows the won tokens

7. The application shows best results

8. The Game moves to the use-case *"Saving results"*, when its over move to step 9

9. The application shows main menu


**Alternative scenarios**:

- A1: **Unexpected application exit**: The game is discarded and the
application closes

**Subscenarios**: None.

**Special requirements**: None.

**Additional information**: During the game, the application remembers the number of currently won tokens for the player







