#include "include/game.h"

Game::Game(Player* player1, Player* player2, const QString &gameName)
    : m_gameName (gameName), m_player1(player1), m_player2(player2)
{
}

QString Game::gameName() const
{
    return m_gameName;
}

void Game::setGameName(const QString &gameName)
{
    m_gameName = gameName;
}

Player *Game::player1() const
{
    return m_player1;
}

void Game::setPlayer1(Player *player1)
{
    m_player1 = player1;
}

Player *Game::player2() const
{
    return m_player2;
}

void Game::setPlayer2(Player *player2)
{
    m_player2 = player2;
}

//QMap<Player, int> Game::tokensWonByPlayer() const
//{
//    return m_tokensWonByPlayer;
//}

//void Game::setTokensWonByPlayer(const Player &player, int tokens)
//{
//    m_tokensWonByPlayer[player] = tokens;
//}
