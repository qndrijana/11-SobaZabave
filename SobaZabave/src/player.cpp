#include "include/player.h"

Player::Player(const QString &username, const QString &password)
    : m_username(username), m_password(password), m_refillTime(QDateTime::currentDateTime().addDays(m_refillCycleDays))
{

}

QString Player::username() const
{
    return m_username;
}

QString Player::password() const
{
    return m_password;
}

int Player::currentTokens() const
{
    return m_currentTokens;
}

void Player::updateTokens(const int newTokens)
{
    //a player cannot have negative number of tokens?
    int updatedTokens = m_currentTokens + newTokens;
    m_currentTokens = updatedTokens >=0 ? updatedTokens : 0;
}

QDateTime Player::currentRefillTime() const
{
    return m_refillTime;
}


void Player::updateRefillTime()
{
    m_refillTime = m_refillTime.addDays(m_refillCycleDays);
}



void Player::refillTokens(int numOfTokens)
{
    while(QDateTime::currentDateTime() > currentRefillTime()){
        updateTokens(numOfTokens);
        updateRefillTime();
    }
}

int Player::refillCycleDays() const
{
    return m_refillCycleDays;
}

void Player::setRefillCycleDays(int refillCycleDays)
{
    m_refillCycleDays = refillCycleDays;
}
