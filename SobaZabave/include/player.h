#ifndef PLAYER_H
#define PLAYER_H

#include <QDateTime>


class Player
{
public:
    Player(const QString &username, const QString &password);

    QString username() const;
    QString password() const;
    int currentTokens() const;
    void updateTokens(const int newTokens); //add or subtract
    QDateTime currentRefillTime() const;
    void updateRefillTime();

    void refillTokens(int numOfTokens);

    int refillCycleDays() const;
    void setRefillCycleDays(int refillCycleDays);

private:
    QString m_username;
    QString m_password;

    int m_currentTokens = 200; //initial number of tokens assigned to every user
    QDateTime m_refillTime; //date and time when to refill tokens
    int m_refillCycleDays = 1;

    //int chosenImage; (an image that the user chose to respresent him(multiple images offered))

};

#endif // PLAYER_H
