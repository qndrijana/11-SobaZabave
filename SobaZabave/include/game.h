#ifndef GAME_H
#define GAME_H

#include "include/player.h"
#include <QMap>


class Game
{
public:
    Game(Player* player1, Player* player2, const QString &gameName);
    Game();
    ~Game();

    virtual void startGame() = 0;
    virtual void endGame() = 0;

    virtual void showResult() const = 0;
    virtual void configureGame() = 0;

    QString gameName() const;
    void setGameName(const QString &gameName);


    //QMap<Player, int> tokensWonByPlayer() const;
    void setTokensWonByPlayer(const Player &player, int tokens);

    Player *player1() const;
    void setPlayer1(Player *player1);

    Player *player2() const;
    void setPlayer2(Player *player2);

protected:
    Player *m_player1, *m_player2;
    QString m_gameName;

    //QMap<Player, int> m_tokensWonByPlayer;

};

#endif // GAME_H
