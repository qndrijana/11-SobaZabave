# Project SobaZabave

Soba zabave je igrica koja korisniku omogucava da odabere jednu od ponudjenih igara (neke ce biti kartaske, neke strateske...). Cilj je sakupiti sto vise poena. Osvojeni poeni se sabiraju za svakog ulogovanog korisnika, a postoje i negativni poeni, pa igrac potencijalno mora da menja izabrana vrata ne bi li mogao da sakupi bodove za dalju igru.

## Developers

- [Andrijana Aleksic, 99/2018](https://gitlab.com/qndrijana)
- [Bogdan Markovic, 130/2018](https://gitlab.com/bogdanis)
- [Nikola Borjan, 10/2018](https://gitlab.com/Dzondzi)
- [Marko Bura, 141/2018](https://gitlab.com/markobura)
- [Svetlana Bicanin, 28/2018](https://gitlab.com/SvetlanaBicanin)
